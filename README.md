# K8S Homework 

Tasks: 
1. Install minikube an start a local Kubernetes cluster
2. Launch a Pod using deployment
3. Create a job and a cronjob that prints env vars
4. Create a configmap and a pod that reads and prints the data from configmap
5. Create a secret and a pod that reads and prints the data from secret

commands:

minikube start
kubectl apply -f nginx.yml 
kubectl get services 
kubectl get deployments 

kubectl apply -f job.yml 
kubectl apply -f cronjob.yml
kubectl get jobs 
kubectl get cronjobs

kubectl apply -f configmap.yml 
kubectl get configmap
kubectl apply -f mongo-express.yml 

kubectl apply -f secret.yml 
kubectl get secrets
kubectl apply -f mongo.yml 


